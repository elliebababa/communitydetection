import os
import sys
import time
import pyspark
from pyspark.conf import SparkConf
#conf setup
conf = SparkConf()
conf.setMaster("local[*]").setAppName("graphFrame_example")
#spark context setup
sc = pyspark.SparkContext(conf=conf)
spark = pyspark.sql.SparkSession.builder.appName('gf').config("spark.sql.shuffle.partitions", "4").getOrCreate() #be sure to set default shuffle paritions on small cluster!!!!!!!!!
from graphframes import *

# helper function here
def KMT(byte):
    if byte < 1024:
        return byte , 'B'
    if byte < 1024*1024:
        return byte/1024, 'KB'
    if byte < 1024*1024*1024:
        return byte/1024/1024, 'MB'
    
def edges(x):
    x = x.split()
    yield pyspark.sql.Row(src = x[0], dst = x[1])
    yield pyspark.sql.Row(src = x[1], dst = x[0])
    
def nodes(x):
    x = x.split()
    row = pyspark.sql.Row("id")
    yield row(x[0])
    yield row(x[1])
    
def main(arg):
    print('## begin testing..')
    print('## args:',arg)
    ts = time.time()
    file_path = arg[0]
    out_file_path = arg[1]
    lines = sc.textFile(file_path).filter(lambda x:x[0]!='#'and len(x)>0).persist()
    lines.count()
    print('## reading file time:',time.time()-ts)

    ndd = lines.flatMap(nodes).toDF().cache()
    edd = lines.flatMap(edges).toDF().cache()

    g = GraphFrame(ndd,edd).cache()
    result = g.labelPropagation(maxIter=5)
    rdd = result.rdd.map(lambda x:(x.label,x.id)).persist()
    rdd.collect()
    print('## building graph and LPA time:',time.time()-ts)
    
    def zero(x):
        return [x]
    def append(x_l,a):
        x_l.append(a)
        return x_l
    def extend(xl1,xl2):
        xl1.extend(xl2)
        xl1.sort()
        return xl1
    
    rdd_group = rdd.combineByKey(zero,append,extend)
    rdd_group = rdd_group.map(lambda x:x[1]).sortBy(lambda x:(len(x),x[0]))
    final_result = rdd_group.collect()
    
    with open(out_file_path, 'w') as fp:
        for l in final_result:
            line = str(l)
            line = line.replace('[','')
            line = line.replace(']','')
            line += "\n"
            fp.write(line)
    print("## total time:",time.time()-ts)

if __name__ == "__main__":
    arg = sys.argv[1:]
    main(arg)