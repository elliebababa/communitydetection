'''
test correctness of community detection algorithms 
reference: 
https://github.com/taynaud/python-louvain
https://zhiyzuo.github.io/python-modularity-maximization/doc/quick-start.html

'''
import unittest
import random

import networkx as nx 
import numpy as np 

import community
from Louvain import *
from community.community_louvain import __randomize as randomize
from networkx.algorithms.community import greedy_modularity_communities

from modularity_maximization import partition
from modularity_maximization.utils import get_modularity

from metric import *

class ModularityTest(unittest.TestCase):
    '''
    test for calculating modularity
    '''
    num_of_tests = 2
    
    def test_count_stub(self):
        edges = [[1,2],[2,3],[1,3],[4,5]]
        LV = Louvain(edges)
        self.assertLessEqual((6,6), LV.stubCount([1,2,3]))
        self.assertLessEqual((2,4), LV.stubCount([1,2]))
        self.assertLessEqual((0,3), LV.stubCount([3,4]))
    
    def test_modularity_calculation(self):
        G = nx.karate_club_graph()
        #ref library
        comm_dict = partition(G)
        ref_Q = get_modularity(G, comm_dict)
        #myLV
        myLV = Louvain([])
        adj = {i:set(G.neighbors(i)) for i in list(G.nodes)}
        communities = []
        for comm in set(comm_dict.values()):
            communities.append([node for node in comm_dict if comm_dict[node] == comm])
        my_Q = myLV.getQ(communities, adj)
        self.assertLessEqual(abs(my_Q - ref_Q), 0.000001)
        # another way of calculating modularity
        def cal_mo(communities, adj):
            q = 0
            m_2 = sum(len(adj[i]) for i in adj)
            for com in communities:
                s_in = 0
                s_tot = 0
                for i in com: 
                    s_tot += len(adj[i])
                    for j in com:
                        s_in += int(j in adj[i])
                q += s_in / m_2 - (s_tot / m_2) ** 2
            return q
        self.assertLessEqual(abs(my_Q - cal_mo(communities, adj)), 0.000001)
        
    
class BestPartitionTest(unittest.TestCase):
    '''
    comparison between best partition
    '''
    num_of_tests = 0
    
class MetricTest(unittest.TestCase):
    num_of_tests = 1
    def test_purity(self):
        self.assertEqual(purity([[1],[2]],[[1,2]]),1)
        self.assertEqual(purity([[1,2]],[[1,2]]),1)
        self.assertEqual(purity([[3,5,6,2]],[[5],[2,3],[6]]),.5)
        self.assertEqual(purity([[3],[5,6,2]],[[5],[2,3],[6]]),.5)
        self.assertNotEqual(purity([[3],[5,6,2]],[[5],[2,3],[6]]),purity([[5],[2,3],[6]],[[3],[5,6,2]]))
    
# class Sample(unittest.TestCase):
# # each test must start with test_
#     def test_ing(self):
#         a = 1
#         self.assertEqual(a, 1, 'a is not equal to 1')
 

if __name__ == '__main__':
    unittest.main()