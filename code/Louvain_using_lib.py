import community as community_louvain
import networkx as nx
import matplotlib.pyplot as plt
from metric import *
import time
# data_file="E:/coursesnotes/CommunityDetection/dataset/ucidata-zachary/out.ucidata-zachary"
# ground_file="E:/coursesnotes/CommunityDetection/dataset/ucidata-zachary/karate_club_label"
# comments = '%'

data_file="E:\\coursesnotes\\CommunityDetection\\dataset\\email\\email-Eu-core.txt\\email-Eu-core.txt"
ground_file="E:\\coursesnotes\\CommunityDetection\\dataset\\email\\email-Eu-core-department-labels.txt\\email-Eu-core-department-labels.txt"

data_file="E:\\coursesnotes\\CommunityDetection\\dataset\\dblp\\com-dblp.ungraph.txt"
ground_file="E:\\coursesnotes\\CommunityDetection\\dataset\\dblp\\com-dblp.all.cmty.txt"
comments = '#'
#

if __name__ == "__main__":
    ts = time.time()
    #load graph
    # G = nx.erdos_renyi_graph(30, 0.05)
    # G = nx.karate_club_graph()
    G = nx.read_edgelist(data_file, comments='#')
    #============================first compute the best partition==================================
    print('='*10, 'best partition', '='*10)
    
    partition = community_louvain.best_partition(G)
    print('time:',time.time()-ts)
    communities = []
    for com in set(partition.values()) :
        list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == com]
        communities.append(list_nodes)
    # print('estimated:',communities)

    #ground communities
    
    g_communities = []
    # ground truth given dict format
    ground_partition = {}
    with open(ground_file,'r') as f:
        for l in f:
            l = l.split()
            v, label = str(l[0]), int(l[1])
            ground_partition[v] = label
    for com in set(ground_partition.values()) :
        list_nodes = [nodes for nodes in ground_partition.keys() if ground_partition[nodes] == com]
        g_communities.append(list_nodes)
    # print('ground truth:',g_communities)
    
    # ground truth given list of communities format
    with open(ground_file,'r') as f:
        for l in f:
            l = l.split()
            g_communities.append(l)
    
    #calculate metrics
    print('purity:', purity(communities, g_communities))
    print('f_purity', F_purity(communities, g_communities))
    print('statistics:',static(communities))

    dendrogram = community_louvain.generate_dendrogram(G)
    print('len of dendrogram:', len(dendrogram))
    for level in range(len(dendrogram)) :
        print('--- level ',level)
        p = community_louvain.partition_at_level(dendrogram, level)
        coms = []
        for com in set(p.values()) :
            list_nodes = [nodes for nodes in p.keys() if p[nodes] == com]
            coms.append(list_nodes)
        print('purity:', purity(coms, g_communities))
        print('f_purity', F_purity(coms, g_communities))
        print('statistics:',static(coms))
    #     print("partition at level", level, "is", community_louvain.partition_at_level(dendrogram, level))  # NOQA
    




















#drawing
# size = float(len(set(partition.values())))
# pos = nx.spring_layout(G)
# count = 0.
# communities = []
# for com in set(partition.values()) :
#     count = count + 1.
#     list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == com]
#     communities.append(list_nodes)
#     # nx.draw_networkx_nodes(G, pos, list_nodes, node_size = 20, node_color = str(count / size))


# nx.draw_networkx_edges(G, pos, alpha=0.5)
# plt.show()