'''
@author: Yufen(Ellie) Huang
here is the code for implementing Louvain algorithm from scratch
to fix: 
1. special cases with no edges in the graph
2. cache stub calculation and update stub per iteration
3. weighted version
'''
from metric import *
import time
import numpy as np
from collections import defaultdict
import random
import sys
from copy import deepcopy
import matplotlib.pyplot as plt
_DEBUG = False
_GAIN_MIN = 0.0001 #should be smaller as the size of the graph grows
_track_modularity = []

data_file = '../dataset/ucidata-zachary/out.ucidata-zachary'
ground_file = 'E:/coursesnotes/CommunityDetection/dataset/ucidata-zachary/karate_club_label'

# data_file="E:\\coursesnotes\\CommunityDetection\\dataset\\email\\email-Eu-core.txt\\email-Eu-core.txt"
# ground_file="E:\\coursesnotes\\CommunityDetection\\dataset\\email\\email-Eu-core-department-labels.txt\\email-Eu-core-department-labels.txt"

data_file="E:\\coursesnotes\\CommunityDetection\\dataset\\dblp\\com-dblp.ungraph.txt"
ground_file="E:\\coursesnotes\\CommunityDetection\\dataset\\dblp\\com-dblp.all.cmty.txt"
def LoadData(input_path = data_file):
    # input_path = 'E:\coursesnotes\CommunityDetection\dataset\email\email-Eu-core.txt\email-Eu-core.txt'
    with open(input_path) as f:
        ret = []
        for line in f:
            try:
                l = line.split()
                assert(len(l) == 2)
                ret.append(l)
            except:
                print("line :\'{}\' excepted".format(line.strip('\n'))) 
        return ret

class Louvain():
    def __init__(self, data):
        self.data = data # undirected edges (u,v)
        self.m = len(self.data)
        self.curgraph = self.edgeToAdj(data) # adj list
        self.nodes = list(self.curgraph.keys())
        self.communities = [[i] for i in self.curgraph] # list of sorted nodes
        self.comDict = {n:(c) for c in self.communities for n in c}
    
    def lookupCommunities(self,n,communities = None):
        # unit test, this function should always return the unique possible answer
        if communities is None:
            communities = self.communities
        for community in communities:
            if n in community:
                return community
        assert(False)
        return [n]
    
    def stubCount(self, community):
        #count stubs inside community and adjacent to community 
        countInside = 0
        countAll = 0
        for n in community:
            adj_list = self.curgraph[n]
            countInside += len(adj_list.intersection(set(community)))
            countAll += len(adj_list)
        return countInside, countAll
      
    def edgesCount(self, community):
        #count edges inside community and adjacent to community 
        countInside = 0
        countAll = 0
        for n in community:
            adj_list = self.curgraph[n]
            for adj in adj_list:
                if adj in community:
                    countInside += 1
                countAll += 1
        countInside /= 2
        countAll -= countInside
        return countInside, countAll
    
    def do_break(self):
        # set up rule for breaking tie
        p = random.randint(0,99)
        if p > 20: #20% of switching community
            return True
        else:
            return False
    
    def agglomerate(self):
        m_2 = self.m*2
        while True:  
            nodes = self.nodes.copy()
            current_communities = deepcopy(self.communities)
            current_comDict = {n:(c) for c in current_communities for n in c}
            current_Q = self.getQ(current_communities, self.curgraph)
            # agglomerate order will also affect the outcome
            random.shuffle(nodes)
            for n in nodes:
                #k,i
                ki = len(self.curgraph[n])
                origin_community = current_comDict[n]
                maximum_gain = 0
                modularity_loss = 0
                if True:#len(origin_community) > 1:
                    # modularity loss if n removed from origin community
                    in1, tot1 = self.stubCount(origin_community)
                    in2, tot2 = self.stubCount([i for i in origin_community if i!=n])
                    modularity_loss = in2/m_2 - (tot2/m_2)**2 - (ki/m_2)**2 - (in1/m_2 - (tot1/m_2)**2)
                candidate_community = origin_community # will stay in original community by default
                #calculate modularity gain
                for nei in self.curgraph[n]:
                    community = current_comDict[nei]
                    assert(nei in community)
                    if n in community:
                        continue
                    # using optimized calculation in paper
                    stub_in, stub_all = self.stubCount(community) 
                    #k,i,in
                    ki_in = len(self.curgraph[n].intersection(set(community)))
                    # cur_gain = (stub_in + ki_in)/2/self.m - pow((stub_all + ki)/2/self.m,2) - (stub_in/2/self.m - pow(stub_all/2/self.m,2) - pow(ki/2/self.m,2))
                    cur_gain = (stub_in + ki_in*2)/m_2 - ((stub_all + ki)/m_2)**2 - (stub_in/m_2 - (stub_all/m_2)**2 - (ki/m_2)**2)
                    #calculate modularity gain directly
                    cur_gain2 = self.getQ([community+[n]], self.curgraph) + self.getQ([[i for i in origin_community if i != n]], self.curgraph) - self.getQ([community], self.curgraph) - self.getQ([origin_community], self.curgraph)

                    if cur_gain+modularity_loss > maximum_gain and self.do_break:
                        maximum_gain = cur_gain+modularity_loss
                        candidate_community = community
                if origin_community == candidate_community:
                    continue
                current_ = self.getQ(current_communities,self.curgraph)
                origin_community.remove(n)
                if len(origin_community) == 0:
                    current_communities.remove(origin_community)
                candidate_community.append(n) 
                candidate_community.sort()
                current_comDict[n] = candidate_community
                next_= self.getQ(current_communities,self.curgraph)
                
            next_Q = self.getQ(current_communities, self.curgraph)
            
            if abs(next_Q - current_Q) < _GAIN_MIN:
                break
            else:
                _track_modularity.append(next_Q)
                self.communities = current_communities
            if _DEBUG:
                print('current communities:',self.communities) 
                print('modularity :',_track_modularity)
                plt.plot(_track_modularity)
                plt.ylabel('modularity values')
                plt.show(block=False)
                plt.pause(3)
                plt.close()
                
            
    def comToNode(self):
        pass
    
    def getQ(self, compos, adj_list):
        # compos should be list of list
        assert(isinstance(compos[0],list))
        q = 0
        m_2 = sum(len(adj_list[i]) for i in adj_list)
        for com in compos:
            for i in com: 
                for j in com:
                    a = int(j in adj_list[i])
                    q += a - len(adj_list[i])*len(adj_list[j]) / m_2 
        return q / m_2
    
    def edgeToAdj(self, data):
        adj = defaultdict(set)
        for edges in data:
            u,v = edges
            if u == v: # exclude self-loop from dataset
                continue
            adj[u].add(v)
            adj[v].add(u)
        return adj
            
    def run(self, pass_num = 1):
        self.curgraph = self.edgeToAdj(self.data)
        for i in range(pass_num):
            #agglomerate
            self.agglomerate()
            #build new graph from communities
            self.comToNode()
            #print curgraph info
            self.printCurgraphInfo(i)
    
    def printCurgraphInfo(self, pass_count = None):
        if pass_count is not None:
            print('Pass Count:', pass_count)
        print('Communities Number:', len(self.curgraph))
        print('Communities :', self.communities)
        communities_size = [len(i) for i in self.communities]
        print('Avg size:', (sum(communities_size))/len(communities_size))
        print('largest size:', max(communities_size))
        print('smallest size:', min(communities_size))
        plt.plot(_track_modularity)
        plt.ylabel('modularity values')
        plt.savefig('modularity_curve')
                
        #print('Current graph:', self.curgraph)
 
if __name__ == "__main__":
    arg = sys.argv[1:]
    print('running Louvain, with arg :',arg)
    data = LoadData()
    ts = time.time()
    louvain = Louvain(data)
    louvain.run()
    
    # ground truth given list of communities format
    g_communities = []
    
    # ground truth given dict format
    ground_partition = {}
    # with open(ground_file,'r') as f:
    #     for l in f:
    #         l = l.split()
    #         v, label = str(l[0]), int(l[1])
    #         ground_partition[v] = label
    # for com in set(ground_partition.values()) :
    #     list_nodes = [nodes for nodes in ground_partition.keys() if ground_partition[nodes] == com]
    #     g_communities.append(list_nodes)
    
    with open(ground_file,'r') as f:
        for l in f:
            l = l.split()
            g_communities.append(l)
    
    # print(louvain.communities)
    # print(g_communities)
    
    #calculate metrics
    print('purity:', purity(louvain.communities, g_communities))
    print('f_purity', F_purity(louvain.communities, g_communities))
    print('statistics:',static(louvain.communities))
    
    print('time:',time.time()-ts)
    