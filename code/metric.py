'''
@author: Yufen(Ellie) Huang
'''
import statistics

#===================== statistics of partition ===================================
def static(communities):
    assert(isinstance(communities[0], set) or isinstance(communities[0], list))
    comNum = len(communities)
    communities_size = [len(i) for i in communities]
    avg_size =  sum(communities_size)/len(communities_size)
    var_size = statistics.variance(communities_size)
    max_size = max(communities_size)
    min_size = min(communities_size)
    return {'comNum':comNum, 'avg':avg_size, 'var':var_size, 'max':max_size, 'min':min_size}

#===================== metric without ground truth ================================ 
def normCut(label, adj):
    pass

#===================== metric with ground truth ================================ 
def purity(estimated_communities, ground_communities):
    # notice that this calculation is not symmetric
    #implemet according to https://arxiv.org/ftp/arxiv/papers/1303/1303.5441.pdf
    
    #=================== type check =================================
    if isinstance(estimated_communities[0],list):
        estimated_communities = [set(i) for i in estimated_communities]
    if isinstance(ground_communities[0],list):
        ground_communities = [set(i) for i in ground_communities]
    assert(isinstance(estimated_communities[0], set))
    assert(isinstance(ground_communities[0], set))
    nodes1 = set()
    for com in estimated_communities:
        nodes1 = nodes1.union(com)
    nodes2 = set()
    for com in ground_communities:
        nodes2 = nodes2.union(com)
    # two partitions should include same nodes
    # if (nodes1!=nodes2):
    #     print('nodes1',len(nodes1))
    #     print('nodes2',len(nodes2))
    #     assert(nodes1 == nodes2)
        
    #=================== calculation =================================
    p = [len(i) for i in estimated_communities]
    px = [0] * len(p)
    for i, x in enumerate(estimated_communities):
        largest = 0
        for y in ground_communities:
            largest = max(largest, len(x.intersection(y)))
        px[i] = largest/p[i]
    return sum(px[i]*p[i] for i in range(len(p)))/sum(p) 

def F_purity(estimated_communities, ground_communities):
    # this calculation is symmetric
    p1 = purity(estimated_communities, ground_communities)
    p2 = purity(ground_communities, estimated_communities)
    return 2*p1*p2 / (p1+p2)