'''
@author: Yufen(Ellie) Huang
here is the code for implementing Louvain algorithm from scratch

to fix: 
1. special cases with no edges in the graph
2. cache stub calculation and update stub per iteration
3. weighted version
'''
import time
import numpy as np
from collections import defaultdict
import random
import sys
import statistics
from copy import deepcopy
import matplotlib.pyplot as plt
from metric import *
_DEBUG = False
_GAIN_MIN = 0.0001 #should be smaller as the size of the graph grows
_track_modularity = []

data_file = '../dataset/ucidata-zachary/out.ucidata-zachary'
ground_file = 'E:/coursesnotes/CommunityDetection/dataset/ucidata-zachary/karate_club_label'
comment_sign = '%'
data_file="E:\\coursesnotes\\CommunityDetection\\dataset\\email\\email-Eu-core.txt\\email-Eu-core.txt"
ground_file="E:\\coursesnotes\\CommunityDetection\\dataset\\email\\email-Eu-core-department-labels.txt\\email-Eu-core-department-labels.txt"

# data_file="E:\\coursesnotes\\CommunityDetection\\dataset\\dblp\\com-dblp.ungraph.txt"
# ground_file="E:\\coursesnotes\\CommunityDetection\\dataset\\dblp\\com-dblp.all.cmty.txt"

def LoadData(input_path = data_file):
    # input_path = 'E:\coursesnotes\CommunityDetection\dataset\dblp\com-dblp.ungraph.txt'
    with open(input_path) as f:
        ret = []
        for line in f:
            try:
                l = line.split()
                assert(len(l) == 2)
                assert(line[0] != comment_sign)
                ret.append(l)
            except:
                print("line :\'{}\' excepted".format(line.strip('\n'))) 
        return ret

class Louvain():
    def __init__(self, data):
        self.data = data # undirected edges (u,v)
        # locally used in each pass
        self.curgraph = self.edgeToAdj(data) # adj list {n:{v:w}}
        self.communities = [[i] for i in self.curgraph] # list of sorted nodes
        self.com_k_int = {n:0 for n in self.curgraph}
        self.com_k_tot = {n:len(self.curgraph[n]) for n in self.curgraph} 
        self.comDict = {n:(i) for i,c in enumerate(self.communities) for n in c} 
        # globally maintain data as well as partition results
        self.nodes = list(self.curgraph.keys())
        self.com_label = {n:(i) for i,c in enumerate(self.communities) for n in c} # the label of each community is the exact idx in communities
        #===== dendrogram over iterations
        self.global_dengrogram = []
    
    def lookupCommunities(self,n,communities = None):
        if communities is None:
            communities = self.communities
        for community in (communities):
            if n in community:
                return community
        assert(False)
        return 0
    
    def stubCount(self, community):
        #count stubs inside community and adjacent to community 
        countInside = 0
        countAll = 0
        for n in community:
            adj_list = self.curgraph[n]
            countInside += len(adj_list.intersection(set(community)))
            countAll += len(adj_list)
        return countInside, countAll
          
    def do_break(self):
        # set up rule for breaking tie
        p = random.randint(0,99)
        if p > 20: #20% of switching community
            return True
        else:
            return False
    
    def pass_initial(self, data):
        if len(data[0]) == 3:
            self.curgraph = self.edgeToAdj(data, weighted=True) # adj list {n:{v:w}}
        else:
            self.curgraph = self.edgeToAdj(data) # adj list {n:{v:w}}
        self.communities = [[i] for i in self.curgraph] # list of sorted nodes
        self.com_k_int = dict()
        self.com_k_tot = dict()
        for idx, com in enumerate(self.communities):
            n = com[0]
            self.com_k_int[idx] = 0 if n not in self.curgraph[n] else self.curgraph[n][n]
            self.com_k_tot[idx] = sum(list(self.curgraph[n].values()))
        self.comDict = {n:(i) for i,c in enumerate(self.communities) for n in c} 
    
    def agglomerate(self, data):
        #initial for single pass
        self.pass_initial(data)
        m_2 = sum(list(self.com_k_tot.values()))
        improvemnt = True
        while improvemnt: # merge nodes until no improvements  
            improvemnt = False
            com = list(self.curgraph.keys()) # here com can either nodes in original graph(1 pass) or community nodes(later pass)
            # agglomerate order will also affect the outcome
            random.shuffle(com)
            for n in com:
                #k,i
                # ki = len(self.curgraph[n])
                origin_community = self.comDict[n]
                candidate_community = origin_community # will stay in original community by default
                maximum_gain = 0
                modularity_loss = 0
                #modularity loss
                # in1, tot1 = self.stubCount(origin_community)
                # in2, tot2 = self.stubCount([i for i in origin_community if i!=n])
                in1, tot1 = self.com_k_int[origin_community], self.com_k_tot[origin_community]
                in2, tot2 = in1, tot1
                for nei in self.curgraph[n]:
                    if nei in self.communities[origin_community]:
                        in2 -= self.curgraph[n][nei]*2
                ki = sum(list(self.curgraph[n].values()))
                tot2 -= ki # after moving node out of community
                modularity_loss = in2/m_2 - (tot2/m_2)**2 - (ki/m_2)**2 - (in1/m_2 - (tot1/m_2)**2)
                #calculate modularity gain
                for nei in self.curgraph[n]:
                    community = self.comDict[nei]
                    if community == candidate_community or community == origin_community:
                        continue
                    # using optimized calculation in paper
                    # stub_in, stub_all = self.stubCount(community) 
                    stub_in, stub_all = self.com_k_int[community], self.com_k_tot[community]
                    # k,i,in
                    # ki_in = len(self.curgraph[n].intersection(set(community)))
                    ki_in = 0
                    for nei in self.curgraph[n]:
                        if nei in self.communities[community]:
                            ki_in += self.curgraph[n][nei]
                    # cur_gain = (stub_in + ki_in)/2/self.m - pow((stub_all + ki)/2/self.m,2) - (stub_in/2/self.m - pow(stub_all/2/self.m,2) - pow(ki/2/self.m,2))
                    cur_gain = (stub_in + ki_in*2)/m_2 - ((stub_all + ki)/m_2)**2 - (stub_in/m_2 - (stub_all/m_2)**2 - (ki/m_2)**2)
                   
                    if cur_gain+modularity_loss > maximum_gain and self.do_break:
                        maximum_gain = cur_gain+modularity_loss
                        candidate_community = community
                if origin_community == candidate_community:
                    continue
                improvemnt = True
                self.communities[origin_community].remove(n)
                self.communities[candidate_community].append(n)
                self.comDict[n] = candidate_community
                self.com_k_int[origin_community] = in2
                self.com_k_tot[origin_community] = tot2
                self.com_k_int[candidate_community] += ki_in*2
                self.com_k_tot[candidate_community] += ki
       
            if _DEBUG:
                next_Q = self.getQ(self.communities, self.curgraph)
                _track_modularity.append(next_Q)
                print('current communities:',self.communities) 
                print('modularity :',_track_modularity)
                plt.plot(_track_modularity)
                plt.ylabel('modularity values')
                plt.show(block=False)
                plt.pause(3)
                plt.close()
                
            
    def comToNode(self):
        tuples = list()
        # update label according to new partition
        
        new_label = dict()
        count = 0
        for idx, com in enumerate(self.communities):
            if len(com) > 0:
                new_label[idx] = count
                count += 1
        for n in self.com_label:
            old_lab = self.com_label[n]
            new_lab = new_label[self.comDict[old_lab]]
            self.com_label[n] =  new_lab
            
        # build new graph
        for idx1, com1 in enumerate(self.communities):
            for idx2, com2 in enumerate(self.communities):
                if (idx2 < idx2):
                    continue
                weights = 0
                for n1 in com1:
                    for n2 in com2:
                        if n2 in self.curgraph[n1]:
                            weights += self.curgraph[n1][n2]
                # count weights between two new nodes
                if weights:
                    tuples.append((new_label[idx1],new_label[idx2],weights))
        # reorganize label        
        return tuples
    
    def getQ(self, compos, adj_list):
        # compos should be list of list
        assert(isinstance(compos[0],list))
        actual_compos = [[]] * len(compos) 
        for n in self.comDict:
            for idx, c in enumerate(compos):
                if self.comDict[n] in c:
                    actual_compos[idx].append(n)
        q = 0
        m_2 = sum(len(adj_list[i]) for i in adj_list)
        for com in actual_compos:
            for i in com: 
                for j in com:
                    a = int(j in adj_list[i])
                    q += a - len(adj_list[i])*len(adj_list[j]) / m_2 
        return q / m_2
    
    
    def getQ_2(self,communities):
        q = 0
        m_2 = sum(list(self.com_k_tot.values()))
        for com, c in enumerate(communities):
            s_in = self.com_k_int[com]
            s_tot = self.com_k_tot[com] 
            q += s_in / m_2 - (s_tot / m_2) ** 2
        return q
    
    
    def edgeToAdj(self, data, weighted = False):
        # if weighted is indicated : data should be list of tuples of three, u, v, w
        # else :data = [(u,v)]
        adj = defaultdict(dict)
        self.strN = dict()
        self.count = 0
        def getN(u):
            if type(u) == str:
                if u in self.strN:
                    return self.strN[u]
                else:
                    self.strN[u] = self.count
                    self.count += 1
                    return self.strN[u]
            else:
                return u
        for edges in data:
            if len(edges) == 3:#weighted:
                u,v,w = getN(edges[0]), getN(edges[1]), int(edges[2])
                adj[u][v] = w
                adj[v][u] = w
            else:
                u,v = getN(edges[0]), getN(edges[1])
                adj[u][v] = 1
                adj[v][u] = 1
            # if u == v: # exclude self-loop from dataset
            #     continue
        return adj
            
    def run(self, pass_num = 2):
        data = self.data
        for i in range(pass_num):
            #agglomerate
            self.agglomerate(data)
            #build new graph from communities
            data = self.comToNode()
            #print curgraph info
            self.printCurgraphInfo(i)
    
    def printCurgraphInfo(self, pass_count = None):
        if pass_count is not None:
            print('=='*20)
            print('Pass Count:', pass_count)
            
        communities_num = len(set(self.com_label.values()))
        communities = dict()
        for n in self.com_label:
            com_  = self.com_label[n]
            if com_ in communities:
                communities[com_].append(n)
            else:
                communities[com_] = [(n)]
        expand_communities = list(communities.values())
        print('Communities :', expand_communities)
        print('Communities Number:', communities_num)
        communities_size = [len(i) for i in expand_communities]
        print('Avg size:', (sum(communities_size))/len(communities_size))
        print('variance:', statistics.variance(communities_size))
        print('largest size:', max(communities_size))
        print('smallest size:', min(communities_size))
        plt.plot(_track_modularity)
        plt.ylabel('modularity values')
        plt.savefig('modularity_curve')
                
        #print('Current graph:', self.curgraph)
 
if __name__ == "__main__":
    arg = sys.argv[1:]
    print('running Louvain, with arg :',arg)
    data = LoadData()
    ts = time.time()
    louvain = Louvain(data)
    louvain.run()
    
    # ground truth given list of communities format
    g_communities = []
        # ground truth given dict format
    ground_partition = {}
    with open(ground_file,'r') as f:
        for l in f:
            l = l.split()
            v, label = str(l[0]), int(l[1])
            ground_partition[v] = label
    for com in set(ground_partition.values()) :
        list_nodes = [nodes for nodes in ground_partition.keys() if ground_partition[nodes] == com]
        g_communities.append(list_nodes)
    
    with open(ground_file,'r') as f:
        for l in f:
            l = l.split()
            g_communities.append(l)
    
    # print(louvain.communities)
    # print(g_communities)
    
    #calculate metrics
    # print('purity:', purity(louvain.communities, g_communities))
    # print('f_purity', F_purity(louvain.communities, g_communities))
    # print('statistics:',static(louvain.communities))
    
    print('time:',time.time()-ts)
    